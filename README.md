# Tutorium zum DIT 2020 - Erstellen einer Consent-Ressource in FHIR R4

In diesem Tutorium im Rahmen des [Deutschen Interoperabilitätstages (DIT) 2020](https://www.interop-tag.de/) werden die Schritte zum  Erstellen einer Consent-Ressource in FHIR R4 gezeigt.

## Tools

- Postman: 								[https://www.postman.com/](https://www.postman.com/)
- Fhir Server (R4), für den DIT 2020:	[https://dit-fhir.molit.eu/fhir/](https://dit-fhir.molit.eu/fhir/)
- Fhir Test Server (R4), öffentlich: 	[http://hapi.fhir.org/baseR4](http://hapi.fhir.org/baseR4)

## Vorbereitung
Installieren und starten Sie Postman oder nutzen Sie die Postman Web-Version (ungetestet).
Melden Sie sich mit Ihrem Postman Account (ggf. zuvor erzeugen) an.

Erstellen Sie einen neuen Request mit dem Namen "POST Consent".

FHIR Ressourcen können in verschiedenen Formaten repräsentiert werden, z.B. in JSON oder XML. Anschließend können Sie im Folgenden zwischen diesen beiden Formaten wählen, z.B. wenn Sie Inhalte an einen FHIR Server senden.
Wechseln Sie in den Bereich "Headers" und passen Sie dort die Header an, je nachdem ob Sie .xml oder .json nutzen wollen, müssen folgende Headers gesetzt sein:   

| Header| .json | .xml |
| -------- | -------- | -------- |
| Accept     | application/fhir+json | application/fhir+xml |
| Content-Type | application/fhir+json | application/fhir+xml |

![Gif](/resources/01_consent_headers.gif)

In diesem Tutorium verwenden wir im Folgenden das JSON Format.

Eventuell müssen außerdem die Header für Content-Length und Host auf *\<calculated when request is sent\>* gesetzt werden.

# Erstellen einer FHIR-Consent-Ressource
FHIR-Consent-Ressource müssen bestimmte Bedingungen erfüllen, genaueres finden Sie hier:
- https://www.hl7.org/fhir/consent.html   
Im Folgenden Abschnitt werden alle notwendigen Schritte zum Erstellen einer Consent-Ressource in FHIR R4 durchgegangen.

### Aufgabe 1 - Versuchen Sie einen Consent-Ressource anzulegen
Ändern Sie den Typ der REST-Anfrage auf POST.

Geben Sie in dem Feld daneben "Request-URL" die Adresse zu einem FHIR R4 Server ein und hängen Sie */Consent* an. Im Fall des FHIR Servers für den DIT-2020 ist die Request-URL also: https://dit-fhir.molit.eu/fhir/Consent

Wechseln in den Bereich "Body" und kopieren Sie die den Inhalt von [/json/consent.json](json/consent.json) in das Eingabefeld.

Sehen Sie sich _"text"_, _"scope"_, _"category"_, _"datetime"_, _"policy"_ und _"provision"_ der FHIR Consent Ressource an. Ändern Sie ggf. _"text"_ und _"datetime"_.

Schicken Sie Ihren POST-Request ab.  

![Gif](/resources/02_consent_post1.gif)  

Sie werden eine Fehlermeldungen mit dem Code *400 Bad Request* mit einer entsprechenden Erläuterung (_Response_) erhalten.

Eine Consent-Ressource sollte immer mit einem Patient, für den Sie gilt, und mit einer Organisation, die Sie verwaltet erstellt werden. Daher bearbeiten wir unsere Consent Ressource in den nächsten Aufgaben und sorgen dafür, dass die notwendigen Ressourcen vorliegen.

### Aufgabe 2 - Patient bearbeiten
Um den Fehler bzgl. der Verknüpfung zur Ressource Patient zu beheben haben Sie die Möglichkeit entweder einen bereits vorhandenen Patienten hinzuzufügen, siehe Abschnitt "Patient hinzufügen", oder einen neuen Patienten anzulegen und diesen anschließend hinzuzufügen, siehe Abschnitt "Patient anlegen".

#### Patient hinzufügen
Durchsuchen *(Strg+F)* Sie den Body Ihres POST-Requests nach *yourID*.
In unserer Beispiel Consent-Ressource sind dies die Platzhalter für Ihre Patienten- bzw. Organisation-IDs.  

![](https://i.imgur.com/HadWBen.png)

Ersetzen Sie *Patient/yourID* durch eine bereits auf dem Server verfügbare Patienten-ID (z.B.: 3301, 3306, 3313, 3321, 3327, usw.).   
Sie können dazu gern ein Patienten aus einem früheren Beispiel nutzen. Falls kein (passender) Patient vorhanden ist, legen Sie einen neuen Patienten an, siehe Abschnitt "Patient anlegen".

Sie können nun zum Request "Post Consent" wechseln und im Body *yourID* in *Patient/yourID* durch diese ersetzen. Fahren Sie anschließend mit "Aufgabe 3 - Organisation anlegen" fort.

#### Patient anlegen
Falls kein Patient verfügbar ist, finden Sie unter [/json/patient.json](json/patient.json) einen Beispielpatienten, den Sie entspr. anpassen und verwenden können.

Legen Sie die Patient-Ressource mit Hilfe eines POST-Requests an Ihre Server-Adresse unter */Patient* an.

![Gif](/resources/03_patient_post.gif)

Ein erfolgreiches Erstellen wird durch die Meldung *201 Created* bestätigt.  

Im Body dieser Meldungen finden Sie dann die ID des neu angelegeten Patienten unter *"id":*. Kopieren Sie diese in die Zwischenablage und fahren Sie mit Abschnitt "Patient hinzfügen" fort.

### Aufgabe 3 - Organisation anlegen 
Da auf dem Test Server bisher keine Organisation vorhanden ist, muss diese neu erzeugt werden.

Die Organisation *Medizininformatik Initiative* finden Sie als FHIR-Ressource in [/json/organization_MII.json](json/organization_MII.json).

Öffnen Sie einen neuen POST-Request zu Ihrem Server nach */Organization* (um nicht erneut entspr. Header setzen zu müssen, können Sie den bereits vorhandenen Request kopieren und anschließend modifizieren) und fügen Sie die Beispiel Organisation *Medizininformatik Initiative* dort als Body ein.   

Sie können den Body nun bearbeiten und die Beispieldaten durch die Daten Ihrer Organisation ersetzen.

![Gif](/resources/04_organization_post.gif)

Schicken Sie den POST-Request ab, die Meldung *201 Created* bestätigt das erfolgreiche Erstellen. Im Body der Meldung finden Sie dann die ID zu Ihrer Organisation. Kopieren Sie diese in die Zwischenablage.

Beachten Sie bitte, dass diese Organisations-ID zu belibig vielen Consent-Ressourcen hinzugefügt werden kann.
Es wird zudem aktuell vom FHIR Server nicht geprüft, ob es bereits eine Organisation mit diesem Namen gibt, sodass es dazu kommen kann, dass eine Organisation mehrfach (mit unterschiedlichen IDs) angelegt wird.

### Aufgabe 4 - Organisation der Consent Ressource hinzufügen
Wechseln Sie nun zum Request "Post Consent" und ersetzen Sie im Body *yourID* in *Organization/yourID* durch die Organisations-ID der zuvor angelegten Organisations-Ressource (Zwischenablage).

### Aufgabe 5 - Consent anlegen
Senden Sie die Anfrage nun ergänzt durch Patienten- und Organisations-ID ab.

Die Meldung *201 Created* bestätigt das erfolgreiche Erstellen der Consent-Ressource.

![Gif](/resources/05_consent_post2.gif)

# Darstellung der Patient-Consent Beziehung in clinFHIR
[http://clinfhir.com/](clinFHIR) bietet umfangreiche Möglichkeiten zur Darstellung von FHIR Ressourcen.

Im folgenden Abschnitt wird beschreiben, wie man einen eigenen Server zu clinFHIR hinzugefügt und wie man seine eigenen Ressourcen darstellen lassen kann.

#### Aufgabe 6 - Server zu clinFHIR hinzfügen
Falls Sie bisher den öffentlichen Testserver http://hapi.fhir.org/baseR4  genutzt haben, können Sie unter *"Current servers"* den Eintrag *Public HAPI R4 server* für *"Data Server"* und *"Conformance Server"* auswählen und mit Aufgabe 7 fortfahren ((für *"Terminology Server"* belassen wir die Voreinstellung *OntoserverR4*).   

Falls Sie einen anderen Server genutzt haben, ist es zur Nutzung von clinFHIR notwendig, dass Ihr FHIR Server [https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS](CORS) unterstützt. Ist dies der Fall, können durch *"Add Server"* Ihren Server hinzufügen.  

![Gif](/resources/06_clinFHIR_addServer.gif)

Fügen Sie dazu unter *"URL"* die Adresse Ihres Servers ein (also https://dit-fhir.molit.eu/fhir/ für den DIT 2020 Testserver) und wählen Sie *R4* für *"FHIR Version"* aus.  

Ausserdem ist zur Darstellung der Ressourcen notwending *"Support $everything"* auszuwählen.   

Testen Sie Ihren Server nun und fügen ihn anschließend durch *Add* hinzu.  

Anschließend können Sie den hinzugefügten Server für die Verwendung auswählen, indem Sie unter *"Current servers"* auf *Edit* klicken.

#### Aufgabe 7 - Eigene Ressourcen in clinFHIR finden
Nun können Sie mit dem *"Patient Viewer"* nach Ihrem Patienten suchen.

Klicken Sie dazu auf *"Select Patient"* und geben ihre Patienten-ID aus dem ersten Teil des Tutoriums in das mittlere Feld ein. Durch einen Klick auf *"Load"* wird ihre Patient-Ressource nun geladen und dargestellt.

![Gif](/resources/07_clinFHIR_PatientViewer.gif)

#### Aufgabe 8 - Beziehungen zu Ressourcen einsehen
Sie finden nun die Darstellungen ihrer Patient-Ressource und können über die Felder links alle mit diesem Patienten Verknüpften FHIR-Ressourcen des Servers einsehen.

Besonders interessant ist dabei die Darstellung aller Beziehungen als Graph durch die Schaltfläche *"Resource reference graph"*.   
Dabei wird Ihnen auch die Ressource in verschiedenen Formaten angezeigt, z.B. als Baum, Text, JSON oder XML. 
 
![Gif](/resources/08_clinFHIR_graph.gif)

## Quellen
- [https://www.interop-tag.de/](https://www.interop-tag.de/)
- [https://www.postman.com/](https://www.postman.com/)
- [https://fhir-drills.github.io/simple-patient.html](https://fhir-drills.github.io/simple-patient.html)
- [https://www.hl7.org/fhir/patient-example.json.html](https://www.hl7.org/fhir/patient-example.json.html)
- [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7275462/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7275462/)  
- [http://clinfhir.com/](http://clinfhir.com/)